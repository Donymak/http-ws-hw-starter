export const currentUsers: string[] = ['donymak'];

export const exists = (user: string) => {
    return currentUsers.includes((user))
}

export const addUser = (user: string) => {
    currentUsers.push(user)
}

export const deleteUser = (user: string) => [
    currentUsers.filter(users => users !== user)
]
