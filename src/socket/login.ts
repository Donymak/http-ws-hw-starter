import {Server} from 'socket.io';
import {addUser, exists} from "./users";


export default (io: Server) => {
    io.on('connection', socket => {

        socket.on('USER_JOINING', username => {
            if (exists(<string>username)) {
                socket.emit('NOT_LOGGED', {message: 'User with given name already exists'})
            } else {
                addUser(<string>username)
                socket.emit('LOGGED', {message: "You've successfully Logged In! "})
            }

        });
    });


};