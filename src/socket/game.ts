import {Server} from 'socket.io';
import {MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_FOR_GAME, SECONDS_TIMER_BEFORE_START_GAME} from "./config";
import {currentUsers} from "./users";
import data from "../data";

export const rooms = ['waitingRoom', 'secondRoom']

export const numberOfUsersInRoomMap = new Map<string, number>(rooms.map(roomName => [roomName, 0]));
export const namesMap = new Map<string, string[]>(rooms.map(roomName => [roomName, []]))
export const usersReadyMap = new Map<string, boolean>(currentUsers.map(user => [user, false]))
export const usersProgressMap = new Map<string, number>(currentUsers.map(user => [user, 0]))
export const playingRoomsMap = new Map<string, boolean>(rooms.map(roomName => [roomName, false]));
export const winnersMap = new Map<string, string[]>(rooms.map(roomName => [roomName, []]))

const setUserToRoomMap = (roomName: string, username: string) => {
    const users = namesMap.get(roomName)
    if (users?.includes(username)) {
        return
    }
    users?.push(username)
    const newUsers = namesMap.get(roomName)
    namesMap.set(roomName, newUsers!)
}

const deleteUserFromRoomMap = (roomName: string, username: string) => {
    const users = namesMap.get(roomName)
    users!.splice(users!.indexOf(username), 1)
}

const setUserDataToMaps = (username: string) => {
    usersReadyMap.set(username, false);
    usersProgressMap.set(username, 0);
}

const createResponse = () => {
    return rooms.filter(roomId => !playingRoomsMap.get(roomId)).map(roomId => ({
        roomId,
        usersCount: numberOfUsersInRoomMap.get(roomId),
        users: namesMap.get(roomId),
        isPlaying: playingRoomsMap.get(roomId)
    }))
}

const getRoomData = (roomName) => {
    const users = namesMap.get(roomName)
    const result: object[] = []
    users!.forEach(user => {
        result.push({
            roomName,
            username: user,
            ready: usersReadyMap.get(user),
            progress: usersProgressMap.get(user)
        })
    })
    return result
}

const numberOfUsers = (room) => {
    return {name: room, numberOfUsers: numberOfUsersInRoomMap.get(room)}
}

const reduceQuantityOfUsers = (roomName) => {
    const usersQuantity = numberOfUsersInRoomMap.get(roomName)
    numberOfUsersInRoomMap.set(roomName, usersQuantity! - 1)
}

const increaseQuantityOfUsers = (roomName) => {
    const usersQuantity = numberOfUsersInRoomMap.get(roomName)
    numberOfUsersInRoomMap.set(roomName, usersQuantity! + 1)
}

const leaveRoom = (data, socket) => {
    const roomName: string = data.query.roomName
    const username: string = data.query.username
    reduceQuantityOfUsers(roomName)
    socket.leave(roomName)
    setUserDataToMaps(username)
    deleteUserFromRoomMap(roomName, username)
    socket.broadcast.emit('UPDATE_COUNTERS', numberOfUsers(roomName))
    socket.emit('UPDATE_COUNTERS', numberOfUsers(roomName))
}

const setOppositeStatus = (data) => {
    const username = data.query.username
    const currentStatus = usersReadyMap.get(data.query.username)
    !currentStatus ? usersReadyMap.set(username, true) : usersReadyMap.set(username, false)
}

const getReadyStatus = (roomName) => {
    const users = namesMap.get(roomName)
    const result: object[] = []
    users!.forEach(user => {
        result.push({
            username: user,
            ready: usersReadyMap.get(user),
        })
    })
    return result
}

const checkAllUsersReady = (roomName) => {
    const users = namesMap.get(roomName)
    const result: boolean[] = []
    users?.forEach(user => {
        result.push(usersReadyMap.get(user)!);
    })

    return !result.includes(false);
}

const getGameData = () => {
    const random = Math.floor(Math.random() * data.texts.length)
    const text = data.texts[random]
    return {text, delay: SECONDS_TIMER_BEFORE_START_GAME, timer: SECONDS_FOR_GAME}
}

const updateProgress = (data) => {
    const username: string = data.query.username,
          progress: number = data.query.progress;
    usersProgressMap.set(username, progress)
    return {username, progress: usersProgressMap.get(username)}
}



const sendResultsByTime = (data) => {
    const roomName: string = data.query.roomName;
    const users = namesMap.get(roomName)
    let result: string[] = []
    result = [...usersProgressMap].filter(item => users!.includes(item[0])).sort((item1, item2) => item2[1] - item1[1]).map(item => item[0])
    return result
}

const sendResultsByDone = (data) => {
    const roomName: string = data.query.roomName;
    return winnersMap.get(roomName)
}


const getCurrentRoomId = socket => Object.keys(socket.rooms).find(roomName => namesMap.get(roomName));

export default (io: Server) => {
    io.on('connection', socket => {
        socket.emit('UPDATE_ROOMS', createResponse())

        socket.on('CREATE_ROOM', data => {
            const roomName: string = data.query.roomName
            const username: string = data.query.username
            if (rooms.includes(roomName)) {
                socket.emit('CREATING_ROOM_ERROR', {message: 'Room with this name already exists'})
            } else {
                setUserDataToMaps(username)
                rooms.push(roomName)
                increaseQuantityOfUsers(roomName)
                namesMap.set(roomName, [])
                setUserToRoomMap(roomName, username)
                socket.join(roomName);
                socket.emit('ROOM_JOINED', getRoomData(roomName));
                socket.broadcast.emit('UPDATE_ROOMS', createResponse())
            }
        })

        socket.on('JOIN_ROOM', data => {
            const roomName: string = data.query.roomName,
                  username: string = data.query.username
            setUserDataToMaps(username)
            if (<number>numberOfUsersInRoomMap.get(roomName) >= MAXIMUM_USERS_FOR_ONE_ROOM) {
                socket.emit('FULL_ROOM', {message: 'This room is full, choose another one'})
            } else {
                const prevRoomId = getCurrentRoomId(socket);
                if (roomName === prevRoomId) {
                    return;
                }
                if (prevRoomId) {
                    const newQuantity = <number>numberOfUsersInRoomMap.get(prevRoomId) - 1
                    numberOfUsersInRoomMap.set(prevRoomId, newQuantity)
                    socket.leave(prevRoomId);
                }
                increaseQuantityOfUsers(roomName)
                setUserToRoomMap(roomName, username)
                socket.broadcast.emit('UPDATE_COUNTERS', numberOfUsers(roomName))
                socket.join(roomName);
                socket.to(roomName).emit('UPDATED_ROOM', getRoomData(roomName));
                socket.emit('ROOM_JOINED', getRoomData(roomName))

            }
        });
        socket.on('LEAVE_ROOM', data => {
            leaveRoom(data, socket)
        })

        socket.on('READY', data => {
            const roomName: string = data.query.roomName
            setOppositeStatus(data)
            io.in(roomName).emit('UPDATE_READY', getReadyStatus(roomName));

            if (checkAllUsersReady(roomName)) {
                io.in(roomName).emit('START_GAME', getGameData());
            }
        })

        socket.on('UPDATE_PROGRESS', data => {
            socket.broadcast.emit('PROGRESS_UPDATED', updateProgress(data))
        })

        socket.on('ADD_WINNER', data => {
            const   roomName: string = data.query.roomName,
                    username: string = data.query.username;
            const winners = winnersMap.get(roomName)
            winners!.push(username)
            winnersMap.set(roomName, winners!)
            if (winners!.length === namesMap.get(roomName)!.length) {
                socket.emit('SHOW_WINNERS', sendResultsByDone(data))
            }
        })

        socket.on('GAME_ENDED', data => {
            socket.emit('SHOW_WINNERS',sendResultsByTime(data))
        })

        socket.on("disconnect", () => {
            console.log(`${socket.id} disconnected`);
        });

    });
};