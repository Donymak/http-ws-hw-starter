export const texts = [
  "Everybody knows that there is no shortcut of getting success in our life. Mastery is the another level progress. It's not easy but possible for everyone. Let's talk about on this topic. First if you want to master in any field then decide that is it your interested field or subject or not. If it's your interested field then it will take short time to become master on that filed. And It's not your interested field but it's your need to be master on that field then you have to love that field from bottom of your heart. And you have to give your 110% focus and time to progress on that filed. And if you honestly work on that filed then the day is not very far when you will achieve your goal easily.",
  "Sharks do not have a single bone in their bodies. Rather than bones, sharks have a skeleton made of cartilage. Cartilage is the same type of tough, flexible tissue that makes up human ears and noses.",
  "The stakes are especially high for Africa’s small farmers, who work their fields by hand and are at the mercy of the elements. The predictable weather patterns these farmers depended on in the past have disappeared. This year, late rains in Ghana and neighboring West African countries delayed planting. Then, unusually heavy rainfall at the end of the growing season hampered the harvest. In East Africa, swarms of locusts, fostered by hotter, wetter conditions, devoured a vast expanse of crops.",
  "Your time is limited, so don't waste it living someone else's life. Don't be trapped by dogma – which is living with the results of other people's thinking. -Steve Jobs",
  "Whoever is happy will make others happy too. -Anne Frank"
];

export default { texts };
