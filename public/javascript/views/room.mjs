import {addClass, createElement, removeClass} from '../helpers/domHelper.mjs';

const appendRoomElement = ({
                               name, numberOfUsers, onJoin = () => {
    }
                           }) => {
    const roomsContainer = document.querySelector('#rooms-wrapper');

    const nameElement = createElement({
        tagName: 'div',
        className: 'room-name',
        attributes: {'data-room-name': name},
        innerElements: [name],
    });

    const numberOfUsersString = getNumberOfUsersString(numberOfUsers);
    const connectedUsersElement = createElement({
        tagName: 'div',
        className: 'connected-users',
        attributes: {'data-room-name': name, 'data-room-number-of-users': numberOfUsers},
        innerElements: [numberOfUsersString],
    });

    const joinButton = createElement({
        tagName: 'button',
        className: 'join-btn',
        attributes: {'data-room-name': name},
        innerElements: ['Join'],
    });

    const roomElement = createElement({
        tagName: 'div',
        className: 'room',
        attributes: {'data-room-name': name},
        innerElements: [nameElement, connectedUsersElement, joinButton],
    });

    roomsContainer.append(roomElement);

    joinButton.addEventListener('click', () => onJoin(name));

    return roomElement;
};

const updateNumberOfUsersInRoom = ({name, numberOfUsers}) => {
    const roomConnectedUsersElement = document.querySelector(`.connected-users[data-room-name='${name}']`);
    roomConnectedUsersElement.innerText = getNumberOfUsersString(numberOfUsers);
    roomConnectedUsersElement.dataset.roomNumberOfUsers = numberOfUsers;
};

const setRoomLabel = (element, label) => {
    element.innerHTML = label
}


const changeReadyButton = (ready) => {
    const readyButton = document.getElementById('ready-btn')
    readyButton.innerText = ready ? 'NOT READY' : 'READY'
}

const hideButtons = () => {
    const readyButton = document.getElementById('ready-btn')
    const quitRoomButton = document.getElementById('quit-room-btn');
    addClass(readyButton, 'display-none')
    addClass(quitRoomButton, 'display-none')
}

const showButtons = () => {
    const readyButton = document.getElementById('ready-btn')
    const quitRoomButton = document.getElementById('quit-room-btn');
    removeClass(readyButton, 'display-none')
    removeClass(quitRoomButton, 'display-none')
}

const showElement = (element) => {
	removeClass(element, 'display-none')
}

const hideElement = (element) => {
    addClass(element, 'display-none')
}

const getNumberOfUsersString = numberOfUsers => `${numberOfUsers} connected`;

const removeRoomElement = name => document.querySelector(`.room[data-room-name='${name}']`)?.remove();

const clearRoomsContainer = () => {
    const container = document.querySelector(`#rooms-wrapper`);
    while (container.firstChild) {
        container.firstChild.remove()
    }
};

export {
    appendRoomElement,
    updateNumberOfUsersInRoom,
    removeRoomElement,
    clearRoomsContainer,
    setRoomLabel,
    changeReadyButton,
    showButtons,
    hideButtons,
    showElement,
    hideElement
};
