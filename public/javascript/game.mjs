import {
    appendRoomElement,
    changeReadyButton,
    clearRoomsContainer,
    hideButtons, hideElement,
    setRoomLabel, showButtons,
    showElement,
    updateNumberOfUsersInRoom
} from "./views/room.mjs";
import {showInputModal, showMessageModal, showResultsModal} from "./views/modal.mjs";
import {addClass, removeClass} from "./helpers/domHelper.mjs";
import {appendUserElement, changeReadyStatus, clearUsersContainer, setProgress} from "./views/user.mjs";

const username = sessionStorage.getItem('username');

if (!username) {
    window.location.replace('/login');
}

let localReady = false;
let localProgress = 0;


let socket = io('http://localhost:3002/game');

const gamePage = document.getElementById('game-page');
const createRoomButton = document.getElementById('add-room-btn');
const roomsContainer = document.getElementById('rooms-page');
const roomNameLabel = document.getElementById('room-name');
const roomCountLabel = document.getElementById('users_count');
const quitRoomButton = document.getElementById('quit-room-btn');
const readyButton = document.getElementById('ready-btn');
const countdownTimer = document.getElementById('timer');
const gameTimerWrapper = document.getElementById('game-timer');
const gameTimer = document.getElementById('game-timer-seconds');
const textContainer = document.getElementById('text-container')


const createRoom = (roomName) => {
    socket.emit('CREATE_ROOM', {query: {username, roomName}});
}

const join = (roomName) => {
    socket.emit('JOIN_ROOM', {query: {username, roomName}});
}

const displayRooms = (data) => {
    clearRoomsContainer();
    data.forEach(room => appendRoomElement({name: room.roomId, numberOfUsers: room.usersCount, onJoin: join}));
}

const leave = () => {
    clearUsersContainer();
    addClass(roomsContainer, 'full-screen');
    removeClass(roomsContainer, 'display-none');
    addClass(gamePage, 'display-none');
    const roomName = roomNameLabel.textContent;
    socket.emit('LEAVE_ROOM', {query: {username, roomName}});
}

const displayUsers = (data) => {
    clearUsersContainer()
    data.forEach(user => {
        setRoomLabel(roomNameLabel, user.roomName);
        setRoomLabel(roomCountLabel, `Users in the room :${data.length}`);
        appendUserElement({username: user.username, ready: user.ready, isCurrentUser: username === user.username});
        setProgress({username: user.username, progress: user.progress});
    })
}

const openRoom = (data) => {
    addClass(roomsContainer, 'display-none');
    removeClass(roomsContainer, 'full-screen');
    removeClass(gamePage, 'display-none');
    displayUsers(data);
}

const updateRoom = (data) => {
    displayUsers(data);
}

const setReady = () => {
    localReady = localReady === false;
    changeReadyButton(localReady);
    const roomName = roomNameLabel.textContent;
    socket.emit('READY', {query: {username, roomName}});
}

const updateReady = (data) => {
    data.forEach(user => {
        changeReadyStatus({username: user.username, ready: user.ready});
    })
}

const calculateProgress = (current, all) => {
    const roomName = roomNameLabel.textContent;
    let progress = current / all * 100;
    localProgress = progress;
    const netProgress = Math.floor(progress);
    setProgress({username, progress: netProgress});
    socket.emit('UPDATE_PROGRESS', {query: { username, progress: netProgress }});
    if (netProgress >= 100) {
        socket.emit('ADD_WINNER', {query: { username, roomName}})
    }
}

const handlePressingKey = (chars) => {
    let currentChar = 0;
    const firstChar = document.getElementById(`char_span0`);
    addClass(firstChar, 'underlined');
    document.addEventListener('keydown', function (event) {
        if (event.key === chars[currentChar]) {
            let char = document.getElementById(`char_span${currentChar}`)
            addClass(char, 'typed')
            currentChar++
            let nextChar = document.getElementById(`char_span${currentChar}`)
            addClass(nextChar, 'underlined')
            calculateProgress(currentChar, chars.length - 1)
        }

    });
}

const displayText = (text) => {
    hideElement(countdownTimer)
    showElement(textContainer)
    textContainer.innerText = ''
    let currentChar = 0

    const chars = text.split('')
    chars.forEach(char => {
        const charSpan = document.createElement('span')
        charSpan.setAttribute('id', `char_span${currentChar}`)
        currentChar++
        charSpan.innerText = `${char}`
        textContainer.appendChild(charSpan)
    })

    handlePressingKey(chars);
}

const sendProgress = () => {
    const roomName = roomNameLabel.textContent;
    socket.emit('GAME_ENDED', { query: {roomName}});
    localProgress = 0;
}

const setGameTimer = (time) => {
    gameTimer.innerText = time
    showElement(gameTimerWrapper)
    let seconds = time
    const countdown = setInterval(() => {
        if (seconds <= 1) {
            clearInterval(countdown);
            sendProgress();
        }
        seconds--
        gameTimer.innerText = seconds
    }, 1000)
}

const setCountdown = (data) => {
    countdownTimer.innerText = data.delay
    showElement(countdownTimer)
    let seconds = data.delay
    const countdown = setInterval(() => {
        if (seconds <= 1) {
            clearInterval(countdown);
            displayText(data.text);
            setGameTimer(data.timer);
        }
        seconds--
        countdownTimer.innerText = seconds
    }, 1000)

}

const startGame = (data) => {
    hideButtons();
    setCountdown(data);
}

const updateProgress = (data) => {
    setProgress(data)
}

const setRoomAfterGame = () => {
    showButtons();
    hideElement(countdownTimer);
    hideElement(gameTimerWrapper);
    hideElement(textContainer);
}

const showResults = (data) => {
    showResultsModal({usersSortedArray: data, onClose: setRoomAfterGame})
}

createRoomButton.addEventListener('click', () => showInputModal({title: 'Enter room name', onSubmit: createRoom}));
quitRoomButton.addEventListener('click', () => leave());
readyButton.addEventListener('click', () => setReady());

socket.on('ROOM_JOINED', openRoom);
socket.on('UPDATE_ROOMS', displayRooms);
socket.on('UPDATED_ROOM', updateRoom);
socket.on('UPDATE_READY', updateReady);
socket.on('START_GAME', startGame);
socket.on('PROGRESS_UPDATED', updateProgress);
socket.on('SHOW_WINNERS', showResults)
socket.on('UPDATE_COUNTERS', updateNumberOfUsersInRoom);
socket.on('FULL_ROOM', showMessageModal);
