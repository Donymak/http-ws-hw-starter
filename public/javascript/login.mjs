import {showMessageModal} from "./views/modal.mjs";
// import {io} from "socket.io-client";

const username = sessionStorage.getItem('username');

if (username) {
    window.location.replace('/game');
}

const socket = io("http://localhost:3002/login");

const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
    const inputValue = getInputValue();
    if (inputValue) {
        socket.emit('USER_JOINING', inputValue)
    }
};

const onKeyUp = ev => {
    const enterKeyCode = 13;
    if (ev.keyCode === enterKeyCode) {
        submitButton.click();
    }
};

const connectToGame = () => {
    const inputValue = getInputValue();
    sessionStorage.setItem('username', inputValue);
    window.location.replace('/game');
}

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);

socket.on('LOGGED',  connectToGame);
socket.on('NOT_LOGGED', showMessageModal);